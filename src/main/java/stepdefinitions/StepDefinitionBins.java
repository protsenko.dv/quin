package stepdefinitions;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import general.BinsApi;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.util.List;

import static java.util.Arrays.stream;
import static managers.ConfigFileManager.getTestUserToken;
import static org.junit.Assert.*;
import static utils.CustomAsserts.assertEventually;
import static utils.ValuesStorage.getCreatedBinId;

public class StepDefinitionBins extends BinsApi {
    private static final Logger log = LogManager.getLogger(StepDefinitionBins.class);

    @After
    public void after(Scenario scenario) {
        log.info("Scenario \"" + scenario.getName() + "\" is finished with the status: " + scenario.getStatus());
    }

    @Given("^no bins with names \"(.*)\" under user \"(.*)\"$")
    public void noBinUnderUser(String binNames, String userName) {
        stream(binNames.split(","))
                .forEach(binName -> deleteUsersBinsByName(getTestUserToken(userName), binName));
    }

    @When("^User \"(.*)\" deletes bins with names \"(.*)\"$")
    public void deleteBinForUser(String userName, String binNames) {
        noBinUnderUser(binNames, userName);
    }

    @When("^User \"(.*)\" creates a bin with name \"(.*)\" and privacy \"(true|false|default)\"$")
    public void userCreatesBinAndStoreId(String userName, String binName, String privacy) {
        createBinAndSaveIdToValueStorage(getTestUserToken(userName), binName, privacy);
    }

    @Then("^Validation Failed when the user \"(.*)\" creates a bin with empty \"body\" parameter$")
    public void validationFailedWhenCreatesBinWithEmptyContent(String userName) {
        Response response = createBinEmptyContent(getTestUserToken(userName));
        assertEquals("The validation is not failed. Response code is " + response.statusCode(), 400,
                response.statusCode());
    }

    @When("^User \"(.*)\" edits a name of bin \"(.*)\" from storage to new bin name \"(.*)\"$")
    public Response userEditsBinNameFromStorage(String userName, String oldBinName, String newBinName) {
        String binId = getCreatedBinId().get(oldBinName);
        Response response = editBinName(getTestUserToken(userName), binId, newBinName);
        getCreatedBinId().put(newBinName, binId);
        return response;
    }

    @When("^User \"(.*)\" edits a content of bin \"(.*)\" from storage to new bin content \"(.*)\"$")
    public Response userEditsBinContentFromStorage(String userName, String binName, String newBinContent) {
        JSONObject newJson = new JSONObject().put("content", newBinContent);
        return updateBin(getTestUserToken(userName), getCreatedBinId().get(binName), newJson);
    }

    @Then("^User \"(.*)\" successfully edited a name of bin \"(.*)\" from storage to new bin name \"(.*)\"$")
    public void userSuccessfullyEditsBinNameFromStorage(String userName, String oldBinName, String newBinName) {
        Response response = userEditsBinNameFromStorage(userName, oldBinName, newBinName);
        assertEquals("Edition of bin unsuccessful: code " + response.statusCode(), 200, response.statusCode());
        noBinUnderUser(oldBinName, userName);
        theBinIsDisplayed(userName, newBinName);
    }

    @Then("^User \"(.*)\" successfully edited a privacy of bin \"(.*)\" from storage and make privacy \"(true|false)\"$")
    public void userSuccessfullyEditsPrivacyFromStorage(String userName, String binName, String privacy) {
        Response response = editBinPrivacy(getTestUserToken(userName), getCreatedBinId().get(binName), privacy);
        assertEquals("Edition of bin unsuccessful: code " + response.statusCode(), 200, response.statusCode());
        assertEquals("The bin privacy still old", privacy, new JSONObject(
                getBinById(getTestUserToken(userName), getCreatedBinId().get(binName)).body().asString())
                .getJSONObject("metadata")
                .get("private").toString());
    }

    @Then("^User \"(.*)\" successfully edited a content of bin \"(.*)\" from storage to new bin content \"(.*)\"$")
    public void userSuccessfullyEditsBinContentFromStorage(String userName, String binName, String newBinContent) {
        Response response = userEditsBinContentFromStorage(userName, binName, newBinContent);
        assertEquals("Edition of bin unsuccessful: code " + response.statusCode(), 200,
                response.statusCode());
        assertEquals("The bin content still old", newBinContent, new JSONObject(
                getBinById(getTestUserToken(userName), getCreatedBinId().get(binName)).body().asString())
                .getJSONObject("record")
                .optString("content"));
    }

    @Given("^a \"(public|private)\" bin \"(.*)\" under user \"(.*)\"$")
    public void givenPublicBinUnderUser(String privacy, String binName, String userName) {
        deleteUsersBinsByName(getTestUserToken(userName), binName);
        Response response = createBinWithName(getTestUserToken(userName), binName, privacy.equalsIgnoreCase("public") ? "false" : "true");
        getCreatedBinId().put(binName, new JSONObject(response.body().asString()).getJSONObject("metadata").optString("id"));
    }

    @Then("^the user \"(.*)\" can see the bin with binName \"(.*)\"$")
    public void theBinIsDisplayed(String loggedUser, String binName) {
        assertEventually(3, 500, () ->
                assertFalse(isBinNotFound(getTestUserToken(loggedUser),
                        getCreatedBinId().get(binName))));
    }

    @Then("^the user \"(.*)\" can't see the bin with binName \"(.*)\"$")
    public void theBinIsNotDisplayed(String loggedUser, String binName) {
        assertEventually(3, 500, () ->
                assertTrue(isBinNotFound(getTestUserToken(loggedUser), getCreatedBinId().get(binName))));
    }

    @When("^The user \"(.*)\" make the private bin \"(.*)\" public$")
    public void iMakeThePrivateBinPublic(String userName, String binName) {
        makeBinsPublic(getTestUserToken(userName), binName);
    }

    @Then("^user \"(.*)\" can see the bins:$")
    public void seeAllBinsTheCreatedInHour(String loggedUser, List<String> listOfBins) {
        listOfBins.forEach(binName -> this.getAllCurrentUsersBins(getTestUserToken(loggedUser))
                .forEach(bin -> bin.getJSONObject("snippetMeta").optString("name").contains(binName))
        );
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries create a bin$")
    public void errorWhenUserTriesCreateABin(String userName) {
        assertTrue("The bin is created without correct authtoken",
                isRequiredAuthenticationForBinCreating(getTestUserToken(userName)));
    }

    @Then("^Validation Failed when the user \"(.*)\" creates a bin with name contained 129 symbols$")
    public void errorWhenLongBinsName(String userName) {
        assertTrue("The bin is created without correct authtoken",
                binsNameIsTooLong(getTestUserToken(userName),
                        "1234567890qwertyuiop1234567890qwertyuiop1234567890123456789" +
                                "0qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiopasdfghjkw"));
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries edit a bin \"(.*)\"$")
    public void errorWhenUserTriesEditABinOfUser(String loggedUserName, String binName) {
        assertTrue("The bin is edited without correct authtoken",
                isRequiredAuthenticationForBinEditing(getTestUserToken(loggedUserName), getCreatedBinId().get(binName)));
    }

    @Then("^the system returns Requires authentication error when user \"(.*)\" tries delete a bin \"(.*)\"$")
    public void errorWhenUserTriesDeleteABinOfUser(String loggedUserName, String binName) {
        assertTrue("The bin is deleted without correct authtoken",
                isRequiredAuthenticationForBinDeletion(getTestUserToken(loggedUserName), getCreatedBinId().get(binName)));
    }


}
