package managers;


import io.restassured.RestAssured;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static java.lang.String.format;
import static java.lang.System.getProperty;
import static org.apache.logging.log4j.util.Strings.isBlank;


public class ConfigFileManager {

    public static final String PROPERTIES_PATH = "data.properties";
    public static final String BINS_API = "bins.api";
    public static final String UPDATE_PRIVACY_API = "update.privacy.api";
    public static final String COLLECTIONS_API = "collections.api";
    public static final String UPDATE_NAME_API = "update.name.api";
    private static final Logger log = LogManager.getLogger(ConfigFileManager.class);
    private static final String DEFAULT_API_URI = "api.uri";

    static {
        RestAssured.baseURI = getDefaultApiUri();
        log.info("Set Base URI to =" + RestAssured.baseURI);
    }

    public static Properties loadProperties() {
        Properties properties = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(PROPERTIES_PATH))) {
            properties = new Properties();
            properties.load(reader);
        } catch (IOException e) {
            log.error("Not able to Load property file !!");
            e.printStackTrace();
        }
        return properties;
    }

    public static String getPropertyValueByName(String name) {
        return loadProperties().getProperty(name);
    }

    public static String getDefaultApiUri() {
        return getParam("apiUri", getPropertyValueByName(DEFAULT_API_URI));
    }

    public static String getTestUserToken(String userName) {
        switch (userName) {
            case "usertest1":
                return getPropertyValueByName("token1");
            case "usertest2":
                return getPropertyValueByName("token2");
            case "anonymous":
                return null;
            default:
                throw new IllegalArgumentException(format("Unknown userName [%s], User name can be only usertest1,"
                        + " usertest2 or anonymous", userName));
        }
    }

    public static String getMandatoryEnvVariable(String name) {
        String parameter = getProperty(name);
        if (isBlank(parameter)) {
            throw new RuntimeException("Failed to detect:" + name);
        }
        return parameter;
    }

    public static String getParam(String variableName, String defaultValue) {
        String env = System.getenv(variableName);
        if (env != null) {
            variableName = defaultValue;
        }
        String result = System.getProperty(variableName, defaultValue);
        if (result == null) {
            result = defaultValue;
        }
        return result;
    }

}

