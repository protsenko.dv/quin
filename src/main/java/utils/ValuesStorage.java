package utils;

import java.util.HashMap;

import static utils.ValuesStorage.StoredValues.BIN_IDS;

public class ValuesStorage {
    public static ThreadLocal<HashMap<Object, Object>> valueStorage = new ThreadLocal<>();

    public static void setBinValueStorage() {
        saveValueToStorage(BIN_IDS, new HashMap<String, String>());
    }

    private static HashMap<Object, Object> getValueStorage() {
        if (valueStorage.get() == null) {
            valueStorage.set(new HashMap<>());
        }
        return valueStorage.get();
    }

    public static <T> T getValueFromStorage(StoredValues storedValue) {
        return (T) getValueStorage().get(storedValue);
    }

    public static void saveValueToStorage(StoredValues storedValues, Object obj) {
        getValueStorage().put(storedValues, obj);
    }

    public static HashMap<String, String> getCreatedBinId() {
        return getValueFromStorage(BIN_IDS);
    }

    public enum StoredValues {
        BIN_IDS
    }
}
