package runners;

import cucumber.api.CucumberOptions;
import general.BinsApi;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import static managers.ConfigFileManager.getTestUserToken;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = {
                "src/main/resources/Feature/BinsCRUD.feature",
                "src/main/resources/Feature/PrivateBinsAccessibility.feature"
        }
        , glue = "stepdefinitions"
)

public class TestRunner {

    @BeforeClass
    public static void setup() {
        try (PrintStream printStream = new PrintStream(new FileOutputStream(System.getProperty("user.dir")
                + "/logs/test.log", false), false)) {
            System.setOut(printStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BinsApi binsApi = new BinsApi();
        binsApi.deleteAllBins(getTestUserToken("usertest1"));
        binsApi.deleteAllBins(getTestUserToken("usertest2"));

    }


}
