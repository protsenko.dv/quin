package general;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static managers.ConfigFileManager.*;
import static utils.ValuesStorage.getCreatedBinId;
import static utils.ValuesStorage.setBinValueStorage;

public class BinsApi extends AbstractApi {

    private static final Logger logger = LogManager.getLogger(BinsApi.class);

    public BinsApi() {
        setBinValueStorage();
    }

    protected Response getBinById(String authToken, String binId) {
        String uri = getPropertyValueByName(BINS_API) + "/" + binId;
        logger.info(uri);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        return get(headers, uri);
    }

    public List<JSONObject> getAllCurrentUsersBins(String authToken) {
        String uri = getPropertyValueByName(COLLECTIONS_API);
        logger.info(uri);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        Response response = get(headers, uri);
        JSONArray binsOfUser = new JSONArray(response.body().asString());
        return jsonArrayToList(binsOfUser);
    }

    public List<JSONObject> getBinsByName(String authToken, String binName) {
        return getAllCurrentUsersBins(authToken).stream().filter(bin ->
                bin.getJSONObject("snippetMeta").optString("name")
                        .contains(binName)).collect(Collectors.toList());
    }

    protected void deleteUsersBinsByName(String authToken, String binName) {
        getBinsByName(authToken, binName)
                .forEach(bin -> deleteUsersBinById(authToken,
                        bin.optString("record")));
    }

    public void deleteAllBins(String authToken) {
        getAllCurrentUsersBins(authToken)
                .forEach(bin -> deleteUsersBinById(authToken,
                        bin.optString("record")));
    }

    protected Response deleteUsersBinById(String authToken, String binId) {
        String uri = getPropertyValueByName(BINS_API) + "/" + binId;
        logger.info(uri);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        return delete(headers, uri);
    }

    public Response createBinWithName(String authToken, String binName, String privacy) {
        JSONObject json = new JSONObject();
        json.put("content", "123");
        return createBin(authToken, binName, privacy, json);
    }

    public Response createBinEmptyContent(String authToken) {
        JSONObject json = new JSONObject();
        return createBin(authToken, "emptyContent", "false", json);
    }

    private Response createBin(String authToken, String name, String privacy, JSONObject json) {
        String uri = getPropertyValueByName(BINS_API);
        Map<String, String> headers = new HashMap<>();
        if (authToken != null) headers.put("X-Master-Key", authToken);
        headers.put("Content-Type", "application/json");
        if (!privacy.equalsIgnoreCase("default")) headers.put("X-Bin-Private", privacy);
        if (!name.equalsIgnoreCase("EMPTY")) headers.put("X-Bin-Name", name);
        return post(headers, uri, json);
    }

    public void createBinAndSaveIdToValueStorage(String authToken, String binName, String privacy) {
        Response response = createBinWithName(authToken, binName, privacy);
        getCreatedBinId().put(binName,
                new JSONObject(
                        response.body().asString())
                        .getJSONObject("metadata")
                        .optString("id"));
    }

    public void makeBinsPublic(String authToken, String binName) {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        headers.put("X-Bin-Private", "false");
        getBinsByName(authToken, binName).forEach(bin -> {
            String id = bin.optString("record");
            put(headers, getPropertyValueByName(BINS_API) + "/"
                    + id
                    + getPropertyValueByName(UPDATE_PRIVACY_API), bin);
        });
    }

    public Response updateBin(String authToken, String binId, JSONObject newJson) {
        String uri = getPropertyValueByName(BINS_API) + "/" + binId;
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        headers.put("Content-Type", "application/json");
        return put(headers, uri, newJson);
    }

    public Response editBinName(String authToken, String binId, String newName) {
        String uri = getPropertyValueByName(BINS_API) + "/" + binId
                + getPropertyValueByName(UPDATE_NAME_API);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        headers.put("X-Bin-Name", newName);
        return put(headers, uri);
    }

    public Response editBinPrivacy(String authToken, String binId, String privacy) {
        String uri = getPropertyValueByName(BINS_API) + "/" + binId
                + getPropertyValueByName(UPDATE_PRIVACY_API);
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Master-Key", authToken);
        headers.put("X-Bin-Private", privacy);
        return put(headers, uri);
    }

    public Boolean isRequiredAuthenticationForBinCreating(String authToken) {
        Response response = createBinWithName(authToken, "This Bin should not be created", "true");
        logger.info(response.body().asString());
        return response.statusCode() == 401 && new JSONObject(response.body().asString())
                .optString("message").contains("You need to pass X-Master-Key in the header");
    }

    public Boolean isRequiredAuthenticationForBinEditing(String authToken, String binId) {
        Response response = updateBin(authToken, binId, new JSONObject().put("content", "This Bin should not be edited"));
        logger.info(response.body().asString());
        return response.statusCode() == 401 && new JSONObject(response.body().asString())
                .optString("message").contains("Invalid X-Master-Key provided or the bin does not belong to your account");
    }

    public Boolean isRequiredAuthenticationForBinDeletion(String authToken, String binId) {
        Response response = deleteUsersBinById(authToken, binId);
        logger.info(response.body().asString() + " status code is: " + response.statusCode());
        String message = new JSONObject(response.body().asString()).optString("message");
        return (response.statusCode() == 401 && message.contains("Invalid X-Master-Key provided")) ||
                (response.statusCode() == 404 && message.contains("Bin not found or it doesn't belong to your account"));
    }

    public Boolean isBinNotFound(String authToken, String binId) {
        Response response = getBinById(authToken, binId);
        logger.info(response.body().asString() + " status code is: " + response.statusCode());
        String message = new JSONObject(response.body().asString()).optString("message");
        return (response.statusCode() == 401 && message.contains("X-Master-Key is invalid or the bin doesn't belong to your account")) ||
                (response.statusCode() == 404 && message.contains("Bin not found or it doesn't belong to your account"));
    }

    public Boolean binsNameIsTooLong(String authToken, String binName) {
        Response response = createBinWithName(authToken, binName, "true");
        logger.info(response.body().asString());
        return response.statusCode() == 400 && new JSONObject(response.body().asString())
                .optString("message").contains("X-Bin-Name cannot be blank or over 128 characters");
    }
}
