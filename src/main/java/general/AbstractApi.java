package general;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static managers.ConfigFileManager.getDefaultApiUri;

public class AbstractApi {

    protected Response get(Map<String, String> headers, String uri) {
        return RestAssured.with().headers(headers)
                .get(getDefaultApiUri() + uri);
    }

    protected Response delete(Map<String, String> headers, String uri) {
        return RestAssured.with().headers(headers)
                .delete(getDefaultApiUri() + uri);
    }

    protected Response post(Map<String, String> headers, String uri, JSONObject json) {
        return RestAssured.with().headers(headers)
                .body(json.toString()).post(getDefaultApiUri() + uri);
    }

    protected Response put(Map<String, String> headers, String uri, JSONObject json) {
        return RestAssured.with().headers(headers)
                .body(json.toString()).put(getDefaultApiUri() + uri);
    }

    protected Response put(Map<String, String> headers, String uri) {
        return RestAssured.with().headers(headers)
                .put(getDefaultApiUri() + uri);
    }

    protected List<JSONObject> jsonArrayToList(JSONArray jsonArray) {
        return IntStream.range(0, jsonArray.length())
                .mapToObj(jsonArray::get)
                .map(e -> (JSONObject) e)
                .collect(Collectors.toList());
    }


}
