Feature: Bins CRUD
  The users can create, read, update and delete the bins

################################ Create ##################################
  Scenario: User successfully creates a bin
    Given no bins with names "binCreationTest" under user "usertest1"
    When User "usertest1" creates a bin with name "binCreationTest" and privacy "true"
    Then the user "usertest1" can see the bin with binName "binCreationTest"

  Scenario: User successfully creates a bin with 128 symbols
    Given no bins with names "1234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiopasdfghjk" under user "usertest1"
    When User "usertest1" creates a bin with name "1234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiopasdfghjk" and privacy "true"
    Then the user "usertest1" can see the bin with binName "1234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiop1234567890qwertyuiop12345678901234567890qwertyuiopasdfghjk"

  Scenario: User successfully creates a bin without name.
    Given no bins with names "EMPTY" under user "usertest1"
    When User "usertest1" creates a bin with name "EMPTY" and privacy "true"
    Then the user "usertest1" can see the bin with binName "EMPTY"

  Scenario: User successfully creates a private bin. The bin is private by default.
    Given no bins with names "privatByDefault" under user "usertest1"
    When User "usertest1" creates a bin with name "privatByDefault" and privacy "default"
    Then the user "usertest1" can see the bin with binName "privatByDefault"
    And the user "usertest2" can't see the bin with binName "privatByDefault"

  Scenario: User can't create a bin with name longer than 128 symbols. An error returns.
    Then Validation Failed when the user "usertest1" creates a bin with name contained 129 symbols

  Scenario: User can't create a bin with empty "body" parameter. An error returns.
    Then Validation Failed when the user "usertest1" creates a bin with empty "body" parameter

  Scenario: Anonymous user can't create a bin
    Then the system returns Requires authentication error when user "anonymous" tries create a bin

    ################################ Edit ##################################

  Scenario: User successfully edits a name of the bin
    Given no bins with names "toEditName,nameSuccessfullyEdited" under user "usertest1"
    When User "usertest1" creates a bin with name "toEditName" and privacy "true"
    Then User "usertest1" successfully edited a name of bin "toEditName" from storage to new bin name "nameSuccessfullyEdited"

  Scenario: User successfully edits a privacy of the bin to make it public
    Given no bins with names "toEditPrivacy" under user "usertest1"
    When User "usertest1" creates a bin with name "toEditPrivacy" and privacy "true"
    Then User "usertest1" successfully edited a privacy of bin "toEditPrivacy" from storage and make privacy "false"

  Scenario: User successfully edits a content of the bin
    Given no bins with names "toEditContent" under user "usertest1"
    When User "usertest1" creates a bin with name "toEditContent" and privacy "true"
    Then User "usertest1" successfully edited a content of bin "toEditContent" from storage to new bin content "SuccessfullyEdited"

  Scenario: Anonymous user can't edit a bin
    Given no bins with names "anonymousCantEdit" under user "usertest1"
    When User "usertest1" creates a bin with name "anonymousCantEdit" and privacy "true"
    Then the system returns Requires authentication error when user "anonymous" tries edit a bin "anonymousCantEdit"

  Scenario: User can't edit an another's bin
    Given no bins with names "anotherCantEdit" under user "usertest2"
    When User "usertest2" creates a bin with name "anotherCantEdit" and privacy "false"
    Then the system returns Requires authentication error when user "usertest1" tries edit a bin "anotherCantEdit"

    ################################ Delete ##################################
  Scenario: User can delete own private bins
    Given a "private" bin "binToDeletion" under user "usertest2"
    When User "usertest2" deletes bins with names "binToDeletion"
    Then the user "usertest2" can't see the bin with binName "binToDeletion"

  Scenario: User can delete own public bins
    Given a "public" bin "binToDeletionP" under user "usertest1"
    When User "usertest1" deletes bins with names "binToDeletionP"
    Then the user "usertest1" can't see the bin with binName "binToDeletionP"
    Then the user "usertest2" can't see the bin with binName "binToDeletionP"

  Scenario: Anonymous user can't delete a bin
    Given no bins with names "anonymousCantDelete" under user "usertest1"
    When User "usertest1" creates a bin with name "anonymousCantDelete" and privacy "false"
    Then the system returns Requires authentication error when user "anonymous" tries delete a bin "anonymousCantDelete"

  Scenario: User can't delete an another's bin
    Given no bins with names "anotherCantDelete" under user "usertest1"
    When User "usertest1" creates a bin with name "anotherCantDelete" and privacy "false"
    Then the system returns Requires authentication error when user "usertest2" tries delete a bin "anotherCantDelete"
