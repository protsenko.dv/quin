Feature: Private Bins Accessibility
  Logged user can create two kinds of bins: public and secret and change their accessibility.
  Anonymous user can read the public bins only.

  Scenario:  The private bin is not displayed for anonymous user
    Given no bins with names "bin_quin_1" under user "usertest1"
    When User "usertest1" creates a bin with name "bin_quin_1" and privacy "true"
    Then the user "usertest1" can see the bin with binName "bin_quin_1"
    Then the user "anonymous" can't see the bin with binName "bin_quin_1"

  Scenario: The public bin is displayed for all users
    Given no bins with names "bin_quin_2" under user "usertest2"
    When User "usertest1" creates a bin with name "bin_quin_2" and privacy "false"
    Then the user "usertest1" can see the bin with binName "bin_quin_2"
    Then the user "usertest2" can see the bin with binName "bin_quin_2"
    Then the user "anonymous" can see the bin with binName "bin_quin_2"

  Scenario: For logged user the private bin of another user is not displayed
    Given no bins with names "bin_quin_3" under user "usertest1"
    When User "usertest1" creates a bin with name "bin_quin_3" and privacy "true"
    Then the user "usertest1" can see the bin with binName "bin_quin_3"
    Then the user "usertest2" can't see the bin with binName "bin_quin_3"

  Scenario: For logged user the public bin of another user is displayed
    Given no bins with names "bin_quin_4" under user "usertest2"
    When User "usertest1" creates a bin with name "bin_quin_4" and privacy "false"
    Then the user "usertest2" can see the bin with binName "bin_quin_4"
    Then the user "usertest1" can see the bin with binName "bin_quin_4"

  Scenario: For logged user the public bins are displayed
    Given no bins with names "bin_quin_5.1,bin_quin_5.2" under user "usertest1"
    When User "usertest1" creates a bin with name "bin_quin_5.1" and privacy "false"
    When User "usertest1" creates a bin with name "bin_quin_5.2" and privacy "false"
    Then user "usertest2" can see the bins:
      | bin_quin_5.1 | bin_quin_5.2 |

  Scenario: For logged user the private bins is not displayed before changing of the publicity status
    Given a "private" bin "bin_quin_6" under user "usertest2"
    Then the user "usertest2" can see the bin with binName "bin_quin_6"
    Then the user "usertest1" can't see the bin with binName "bin_quin_6"
    When The user "usertest2" make the private bin "bin_quin_6" public
    Then the user "usertest1" can see the bin with binName "bin_quin_6"

