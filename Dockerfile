FROM maven:3.6.3-jdk-11

WORKDIR /tmp/test
ENTRYPOINT ["/bin/sh"]
CMD ["-c", "mvn clean install serenity:aggregate"]
