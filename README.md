# Quin QA Engineer assignment #

Test Automation framework for Quin's QA Test Automation assignment created specifically for company recruitment process.
Some tests for JSONBIN.io Bins API have been automated. However, the project provides an example of the architecture of a test automation framework to test API in a BDD framework implemented using Cucumber, Serenity and Rest Assured.

## Overview ##
This test framework includes api tests for GitHub Bins API. The framework is based on BDD achieved using Rest Assured library to handle the API tests:<br>
https://api.jsonbin.io/v3/
## Test Scenarios ##
All test Scenarios are here: src/main/resources/Feature in the files BinsCRUD.feature, PrivateBinsAccessibility.feature

## How To Run ##
        In case you have acess to https://gitlab.com/protsenko.dv/quin/-/pipelines
        1. go to CI/CD press "Run pipeline" button

        In case you have Linux and installed Docker on your local machine:
        1. clone the project
        2. go to the project directory
        3. execute shell script -  run.sh
                ./run.sh
                
        In case you have Windows and installed Docker:
        1. go to the project directory right now
        2. Find the script run.sh, open it
        3. Please comment (by #) the row: docker run --user=`id -u` -v `pwd`:/tmp/test -it testrunner:local
        4. Please uncomment row: docker run -v ${pwd}:/tmp/test -it testrunner:local
        5. save the file
        6. run script ./run.sh
        
        In case you have Linux and installed java 8 (and higher) and maven 3:
        1. go to the project directory
        2. run command: 
            mvn clean install serenity:aggregate

## Jsonbin.io Tokens ##
For correct work of the tests the jsonbin.io tokens are specified as variables in data.properties file. You can generate your own or take the generated from data.properties

## Configuration file ##
Please find the configuration file in the project directory: data.properties

## Reporting ##
The reporting tool is serenity.
After the running of tests please find the report:

        In case you are on https://gitlab.com/protsenko.dv/quin/-/pipelines
        1. Open last build
        2. Press to "deploy" button
        3. Browse the job artifact
        4. Open the target/site/serenity/index.html
        5. accept the unsecure connection

        In case you run tests on your local machine:
        1. open target/site/serenity/index.html

## Logs ##
After the running of tests, please find the logs in the project directory: *logs*


# Tools Used
Programming Language - Java<br/>
Framework Details - BDD using Cucumber and Gherkin Syntax, Rest-assured<br/>
Reporting - Serenity<br/>
JSON libraries - for manipulating JSON data<br/>

# Had to do
The task is to create a series of automated tests that cover the CRUD operations of jsonbin API. You will start with documentation page here: https://jsonbin.io/api-reference (Bins API).

We are expecting to see the test scenarios implemented with Kotlin/ Java and REST Assured. We will be happy to see Cucumber implementation but is not a must. What we want to see is:
-  How you test APIs;
-  Programming skills (OOP);
-  Good structuring of tests for reusability and maintainability;
-  Gitlab CI platform is a plus.
-  README file 
-  Push your solution to Gitlab and send us the link. In case you have any doubts, please contact us.
